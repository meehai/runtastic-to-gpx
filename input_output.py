import tempfile
import os
from zipfile import ZipFile
from datetime import datetime
import json

from time_utils import formatTimestampForOutput

def readAndExtractArchive(archivePath):
	tmpDirPath = tempfile.mkdtemp()

	# Read and extract data to tmp file
	zipFile = ZipFile(archivePath)
	zipFile.extractall(path=tmpDirPath)
	baseDir = "%s/Sport-sessions" % (tmpDirPath)
	return baseDir

def getRunsNames(baseDir):
	return list(filter(lambda x : x.endswith(".json"), os.listdir(baseDir)))

def readJSONFiles(basePath, fileName):
	def readMergeAndConvert(path):
		return json.loads("".join(open(path).readlines()))

	try:
		runFile = readMergeAndConvert("%s/%s" % (basePath, fileName))
		gpsFile = readMergeAndConvert("%s/GPS-data/%s" % (basePath, fileName))
		elevationFile = readMergeAndConvert("%s/Elevation-data/%s" % (basePath, fileName))
	except Exception as e:
		print(e)
		return None
	return runFile, gpsFile, elevationFile

def writeArrayToGpx(gpsEleData, startTime, outputPath):
	# Header
	Str = '''
<?xml version="1.0" encoding="UTF-8"?>
<gpx version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <metadata>
    <link href="http://www.garmin.com">
      <text>Garmin International</text>
    </link>
''' + \
	"    <time>%s</time>" % (formatTimestampForOutput(startTime)) + \
'''
  </metadata>
  <trk>
    <name>Runtastic Run</name><type>Run</type>
    <trkseg>
'''

	# Content
	for i in range(len(gpsEleData)):
		item = gpsEleData[i]
		strItem = "      <trkpt lon=\"%s\" lat=\"%s\">\n" % (item[1], item[2])
		strItem += "        <ele>%d</ele>\n" % (item[3])
		strItem += "        <time>%s</time>\n" % formatTimestampForOutput(item[0])
		strItem += "      </trkpt>\n"
		Str += strItem

	# Footer
	Str += '''    </trkseg>
  </trk>
</gpx>
'''
	fileName = "%s/%s.gpx" % (outputPath, str(datetime.fromtimestamp(startTime)))
	f = open(fileName, "w")
	f.write(Str)
	f.flush()
	f.close()
	print("Exported to %s" % (fileName))