import numpy as np
from input_output import readJSONFiles
from time_utils import convertTimestampToEpoch

def matchGpsAndElevation(gpsFile, elevationFile, startTime):
	prevJ = 0
	gpsEleData = []
	for i in range(len(gpsFile)):
		gpsItem = gpsFile[i]
		found = False
		# Make sure we start from previous item
		for j in range(prevJ, len(elevationFile)):
			eleItem = elevationFile[j]
			# Match timestamp strings
			if gpsItem["timestamp"] == eleItem["timestamp"]:
				found = True
				prevJ = j + 1
				break

		if found:
			timeStamp = convertTimestampToEpoch(gpsItem["timestamp"])
			newItem = [timeStamp, gpsItem["longitude"], gpsItem["latitude"], eleItem["elevation"]]
			# Ignore errorneous data recorded before the start of the run
			if timeStamp >= startTime:
				gpsEleData.append(newItem)

	gpsEleData = np.array(gpsEleData, dtype=np.float64)
	return gpsEleData

def smoothDuplicateEntries(gpsEleArray):
	def applySmoothing(items):
		first, last, count = items[0], items[-1], len(items)
		a = np.linspace(first, last, count + 1)[1 : ]
		a[:, 0] = np.ceil(a[:, 0])
		return a

	badIndices = []
	thisBucket = []
	countSmoothed = 0
	for i in range(1, len(gpsEleArray)):
		if np.abs(gpsEleArray[i - 1, 1 : ] - gpsEleArray[i, 1 : ]).sum() == 0:
			thisBucket.append(i)
			countSmoothed += 1
		else:
			if len(thisBucket) > 0:
				thisBucket.append(i)
				badIndices.append(thisBucket)
			thisBucket = []

	print("Smoothing %d entries" % (countSmoothed))
	for i in range(len(badIndices)):
		thisBucket = badIndices[i]
		gpsEleArray[thisBucket] = applySmoothing(gpsEleArray[thisBucket])
		
	return gpsEleArray

def addMissingTimeEntries(gpsEleArray):
	def applySmoothing(left, right):
		timeDiff = int(right[0] - left[0]) - 1
		newItems = np.zeros(shape=(timeDiff, 4), dtype=np.float64)
		newItems[:] = left
		return newItems

	timeDiffs = gpsEleArray[1 :, 0] - gpsEleArray[0 : -1, 0]
	whereBigDiffs = np.where(timeDiffs > 1)[0]

	# Store an index where to add and the smoother sub-array
	smoothedArrays = []
	for i in range(len(whereBigDiffs)):
		ix = whereBigDiffs[i]
		result = applySmoothing(gpsEleArray[ix], gpsEleArray[ix + 1])
		smoothedArrays.append((ix, result))

	# Add each subarray at the designated index, counting also how many were added so far
	howManyAddedSoFar = 0
	for i in range(len(smoothedArrays)):
		whereIx = smoothedArrays[i][0] + howManyAddedSoFar
		addedArray = smoothedArrays[i][1]
		gpsEleArray = np.concatenate([gpsEleArray[0 : whereIx], addedArray, gpsEleArray[whereIx : ]])
		howManyAddedSoFar += len(addedArray)

	timeDiffs = gpsEleArray[1 :, 0] - gpsEleArray[0 : -1, 0]
	whereBigDiffs = np.where(timeDiffs > 1)[0]
	print("Added %d dummy entries to linearize time" % (howManyAddedSoFar))

	return gpsEleArray

def convertOne(baseDir, fileName):
	res = readJSONFiles(baseDir, fileName)
	if not res:
		return None
	runFile, gpsFile, elevationFile = res
	assert runFile["start_time_timezone_offset"] == runFile["end_time_timezone_offset"], \
		"Tool doesn't support start and end time on different timezones."

	startTime = runFile["start_time"] // 1000
	gpsEleArray = matchGpsAndElevation(gpsFile, elevationFile, startTime)
	print("Run: %s. %d GPS/elevation matches found (Total GPS: %d | Elevation: %d)" % \
		(fileName, len(gpsEleArray), len(gpsFile), len(elevationFile)))

	gpsEleArray = addMissingTimeEntries(gpsEleArray)
	gpsEleArray = smoothDuplicateEntries(gpsEleArray)
	return gpsEleArray, startTime