import json
import sys
import os
import numpy as np
from datetime import datetime

from zipfile import ZipFile
import tempfile
import shutil

def getTimeStampOffset(strTimeStamp):
	wherePlus = strTimeStamp.index("+")
	whereMinus = strTimeStamp.rindex("-")
	if wherePlus > 0:
		offset = strTimeStamp[wherePlus + 1: ]
		offset = int(offset[-2 :]) * 60 + int(offset[0 : 2]) * 3600
		strTimeStamp = strTimeStamp[0 : wherePlus - 1]
	elif whereMinus > 0:
		offset = strTimeStamp[whereMinus + 1: ]
		offset = -(int(offset[-2 :]) * 60 + int(offset[0 : 2]) * 3600)
		strTimeStamp = strTimeStamp[0 : whereMinus - 1]
	else:
		offset = 0
	return strTimeStamp, offset

# Converts from string to epoch time
def convertTimestampToEpoch(strTimeStamp):
	# Get offset and truncated string
	strTimeStamp, offset = getTimeStampOffset(strTimeStamp)

	# Convert to epoch and subtract time zone offset (Strava seems to infer this from GPS position)
	timeStamp = int(datetime.strptime(strTimeStamp, "%Y-%m-%d %H:%M:%S").timestamp()) - offset

	return timeStamp

def readFiles(basePath, fileName):
	def readMergeAndConvert(path):
		return json.loads("".join(open(path).readlines()))

	runFile = readMergeAndConvert("{}/{}".format(basePath, fileName))

	if not os.path.exists("{}/GPS-data/{}".format(basePath, fileName)):
		return None, None, None

	gpsFile = readMergeAndConvert("{}/GPS-data/{}".format(basePath, fileName))
	elevationFile = readMergeAndConvert("{}/Elevation-data/{}".format(basePath, fileName))

	return runFile, gpsFile, elevationFile

def matchGpsAndElevation(gpsFile, elevationFile, startTime):
	prevJ = 0
	gpsEleData = []
	for i in range(len(gpsFile)):
		gpsItem = gpsFile[i]
		found = False
		# Make sure we start from previous item
		for j in range(prevJ, len(elevationFile)):
			eleItem = elevationFile[j]
			# Match timestamp strings
			if gpsItem["timestamp"] == eleItem["timestamp"]:
				found = True
				prevJ = j + 1
				break

		if found:
			timeStamp = convertTimestampToEpoch(gpsItem["timestamp"])
			newItem = [timeStamp, gpsItem["longitude"], gpsItem["latitude"], eleItem["elevation"]]
			# Ignore errorneous data recorded before the start of the run
			if timeStamp >= startTime:
				gpsEleData.append(newItem)

		else:
			if len(gpsEleData) > 0:
				timeStamp = convertTimestampToEpoch(gpsItem["timestamp"])
				newItem = [timeStamp, gpsItem["longitude"], gpsItem["latitude"], gpsEleData[-1][3]]
				gpsEleData.append(newItem)


	gpsEleData = np.array(gpsEleData, dtype=np.float64)
	return gpsEleData

def writeArrayToGpx(gpsEleData, startTime):
	def formatTime(timeStamp):
		item = str(datetime.fromtimestamp(timeStamp))
		item = "{}T{}.000Z".format(item[0 : 10], item[11 : ])
		return item

	# Header
	Str = '''<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="StravaGPX Android" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd" version="1.1" xmlns="http://www.topografix.com/GPX/1/1">
 <metadata>
  <time>{}</time>'''.format(formatTime(startTime)) + \
'''
 </metadata>
 <trk>
  <name>Runtastic Run</name>
  <type>9</type>
  <trkseg>
'''


	# Content
	for i in range(len(gpsEleData)):
		item = gpsEleData[i]
		strItem = "   <trkpt lat=\"{}\" lon=\"{}\">\n".format(item[2], item[1])
		strItem += "    <ele>{}</ele>\n".format(item[3])
		strItem += "    <time>{}</time>\n".format(formatTime(item[0]))
		strItem += "   </trkpt>\n"
		Str += strItem

	# Footer
	Str += '''  </trkseg>
 </trk>
</gpx>
'''
	fileName = "export/{}.gpx".format(datetime.fromtimestamp(startTime))
	fileName = '_'.join(fileName.split(' ')).replace(':', '-')

	f = open(fileName, "w")
	f.write(Str)
	f.flush()
	f.close()
	print("Exported to {}\n".format(fileName))


def addMissingTimeEntries(gpsEleArray):
	def apply_smoothing(left, right):
		missing_count = int(right[0] - left[0]) - 1
		time_steps = np.arange(left[0]+1, right[0], dtype=np.float64)
		longitudes = np.linspace(left[1], right[1], missing_count+2)[1:-1]
		latitudes  = np.linspace(left[2], right[2], missing_count+2)[1:-1]
		elevations = np.linspace(left[3], right[3], missing_count+2)[1:-1]

		new_items = np.stack([time_steps, longitudes, latitudes, elevations], axis=1)
		return new_items


	timeDiffs = gpsEleArray[1 :, 0] - gpsEleArray[0 : -1, 0]
	whereBigDiffs = np.where(timeDiffs > 1)[0]

	# Create new array containing missing values
	newGpsEleArray = np.empty(shape=(0, 4))
	for i in range(len(whereBigDiffs)):
		idx = whereBigDiffs[i]

		prev_idx = whereBigDiffs[i-1] + 1 if i > 0 else 0

		to_add_array = apply_smoothing(gpsEleArray[idx], gpsEleArray[idx+1])

		newGpsEleArray = np.concatenate([newGpsEleArray,
										 gpsEleArray[prev_idx:idx+1],
										 to_add_array])

	# add remaining items from the original end
	newGpsEleArray = np.concatenate([newGpsEleArray, gpsEleArray[whereBigDiffs[-1] + 1:]])

	return newGpsEleArray

def convertOne(baseDir, fileName):
	runFile, gpsFile, elevationFile = readFiles(baseDir, fileName)

	# manual entries don't have gps and elevation data -> skip them
	if runFile == None:
		return

	assert runFile["start_time_timezone_offset"] == runFile["end_time_timezone_offset"], \
		"Tool doesn't support start and end time on different timezones."

	startTime = runFile["start_time"] // 1000
	gpsEleArray = matchGpsAndElevation(gpsFile, elevationFile, startTime)
	print("Run: {}. {} GPS/elevation matches found (Total GPS: {} | Elevation: {})".format(fileName, len(gpsEleArray), len(gpsFile), len(elevationFile)+1))


	def removeDuplicateEntries(array):
		new_array = []
		set_unique = set()
		for x in array:
			if (x[1], x[2]) not in set_unique:
				new_array.append(x)
				set_unique.add((x[1], x[2]))

		if array[-1][0] != new_array[-1][0]:
			new_array[-1][0] = array[-1][0]

		return np.array(new_array)

	gpsEleArray = removeDuplicateEntries(gpsEleArray)
	gpsEleArray = addMissingTimeEntries(gpsEleArray)

	writeArrayToGpx(gpsEleArray, startTime)

def readArchiveAndWriteOutput(archivePath, outputPath):
	tmpDirPath = tempfile.mkdtemp()
	os.mkdir(outputPath) if not os.path.isdir(outputPath) else []

	# Read and extract data to tmp file
	zipFile = ZipFile(archivePath)
	zipFile.extractall(path=tmpDirPath)

	baseDir = "{}/Sport-sessions".format(tmpDirPath)
	allRunFiles = filter(lambda x : x.endswith(".json"), os.listdir(baseDir))
	for item in allRunFiles:
		convertOne(baseDir, item)
	shutil.rmtree(tmpDirPath)

def main():
	os.environ["TZ"] = "UTC"
	# readArchiveAndWriteOutput(sys.argv[1], "export")
	# readArchiveAndWriteOutput("export-20190921-000.zip", "export")
	convertOne("data", "55dabab16cc1cecec9380a71.json")

if __name__ == "__main__":
	main()